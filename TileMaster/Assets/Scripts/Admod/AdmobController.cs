﻿using System;
using System.Collections;
using UnityEngine;
using GoogleMobileAds.Api;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;
using GoogleMobileAds.Common;

public class AdmobController : MonoBehaviour
{
    private readonly TimeSpan APPOPEN_TIMEOUT = TimeSpan.FromHours(4);
    private DateTime appOpenExpireTime;
    private AppOpenAd appOpenAd;
    private BannerView bannerView;
    private InterstitialAd interstitialAd;
    private RewardedAd rewardedAd;
   // private RewardedInterstitialAd rewardedInterstitialAd;
    private float deltaTime;
    private bool isShowingAppOpenAd;
    public UnityEvent OnAdLoadedEvent;
    public UnityEvent OnAdFailedToLoadEvent;
    public UnityEvent OnAdOpeningEvent;
    public UnityEvent OnAdFailedToShowEvent;
    public UnityEvent OnUserEarnedRewardEvent;
    public UnityEvent OnAdClosedEvent;
    
    #region UNITY MONOBEHAVIOR METHODS

    public void Start()
    {
        MobileAds.SetiOSAppPauseOnBackground(true);

        List<String> deviceIds = new List<String>() { AdRequest.TestDeviceSimulator };

        // Add some test device IDs (replace with your own device IDs).
#if UNITY_IPHONE
        deviceIds.Add("");
#elif UNITY_ANDROID
        deviceIds.Add("");
#endif

        // Configure TagForChildDirectedTreatment and test device IDs.
        RequestConfiguration requestConfiguration =
            new RequestConfiguration.Builder()
            .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.Unspecified)
            .SetTestDeviceIds(deviceIds).build();
        MobileAds.SetRequestConfiguration(requestConfiguration);

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(HandleInitCompleteAction);

        // Listen to application foreground / background events.
      //  AppStateEventNotifier.AppStateChanged += OnAppStateChanged;
    }

    private void HandleInitCompleteAction(InitializationStatus initstatus)
    {
        Debug.Log("Initialization complete.");

        // Callbacks from GoogleMobileAds are not guaranteed to be called on
        // the main thread.
        // In this example we use MobileAdsEventExecutor to schedule these calls on
        // the next Update() loop.
        //MobileAdsEventExecutor.ExecuteInUpdate(() =>
        //{          
        //    RequestBannerAd();
        //});
    }


    #endregion

    #region HELPER METHODS

    private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder()
            .AddKeyword("unity-admob-sample")
            .Build();
    }

    #endregion

    #region BANNER ADS

//    public void RequestBannerAd()
//    {
       
//        // These ad units are configured to always serve test ads.
//#if UNITY_EDITOR
//        string adUnitId = "unused";
//#elif UNITY_ANDROID
//        string adUnitId = "ca-app-pub-3940256099942544/6300978111";
//#elif UNITY_IPHONE
//        string adUnitId = "ca-app-pub-3940256099942544/2934735716";
//#else
//        string adUnitId = "unexpected_platform";
//#endif

//        // Clean up banner before reusing
//        if (bannerView != null)
//        {
//            bannerView.Destroy();
//        }

//        // Create a 320x50 banner at top of the screen
//        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

//        // Add Event Handlers
//        bannerView.OnAdLoaded += (sender, args) =>
//        {
          
//            OnAdLoadedEvent.Invoke();
//        };
//        bannerView.OnAdFailedToLoad += (sender, args) =>
//        {
          
//            OnAdFailedToLoadEvent.Invoke();
//        };
//        bannerView.OnAdOpening += (sender, args) =>
//        {
           
//            OnAdOpeningEvent.Invoke();
//        };
//        bannerView.OnAdClosed += (sender, args) =>
//        {
           
//            OnAdClosedEvent.Invoke();
//        };
//        bannerView.OnPaidEvent += (sender, args) =>
//        {
//            string msg = string.Format("{0} (currency: {1}, value: {2}",
//                                        "Banner ad received a paid event.",
//                                        args.AdValue.CurrencyCode,
//                                        args.AdValue.Value);
          
//        };

//        // Load a banner ad
//        bannerView.LoadAd(CreateAdRequest());
//    }

    //public void DestroyBannerAd()
    //{
    //    if (bannerView != null)
    //    {
    //        bannerView.Destroy();
    //    }
    //}

    #endregion

    #region INTERSTITIAL ADS

    public void RequestAndLoadInterstitialAd()
    {
        
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-3940256099942544/1033173712";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/4411468910";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Clean up interstitial before using it
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }

        interstitialAd = new InterstitialAd(adUnitId);

        // Add Event Handlers
        interstitialAd.OnAdLoaded += (sender, args) =>
        {
          
            OnAdLoadedEvent.Invoke();
        };
        interstitialAd.OnAdFailedToLoad += (sender, args) =>
        {
            
            OnAdFailedToLoadEvent.Invoke();
        };
        interstitialAd.OnAdOpening += (sender, args) =>
        {
            OnAdOpeningEvent.Invoke();
        };
        interstitialAd.OnAdClosed += (sender, args) =>
        {
            
            OnAdClosedEvent.Invoke();
        };
        interstitialAd.OnAdDidRecordImpression += (sender, args) =>
        {
            
        };
        interstitialAd.OnAdFailedToShow += (sender, args) =>
        {
           
        };
        interstitialAd.OnPaidEvent += (sender, args) =>
        {
            string msg = string.Format("{0} (currency: {1}, value: {2}",
                                        "Interstitial ad received a paid event.",
                                        args.AdValue.CurrencyCode,
                                        args.AdValue.Value);
           
        };

        // Load an interstitial ad
        interstitialAd.LoadAd(CreateAdRequest());
    }

    public void ShowInterstitialAd()
    {
        if (interstitialAd != null && interstitialAd.IsLoaded())
        {
            interstitialAd.Show();
        }
        else
        {
            
        }
    }

    public void DestroyInterstitialAd()
    {
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }
    }

    #endregion

    #region REWARDED ADS

    public void RequestAndLoadRewardedAd()
    {
        
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-3940256099942544/5224354917";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/1712485313";
#else
        string adUnitId = "unexpected_platform";
#endif

        // create new rewarded ad instance
        rewardedAd = new RewardedAd(adUnitId);

        // Add Event Handlers
        rewardedAd.OnAdLoaded += (sender, args) =>
        {
           
            OnAdLoadedEvent.Invoke();
        };
        rewardedAd.OnAdFailedToLoad += (sender, args) =>
        {
            
            OnAdFailedToLoadEvent.Invoke();
        };
        rewardedAd.OnAdOpening += (sender, args) =>
        {
            
            OnAdOpeningEvent.Invoke();
        };
        rewardedAd.OnAdFailedToShow += (sender, args) =>
        {
            
            OnAdFailedToShowEvent.Invoke();
        };
        rewardedAd.OnAdClosed += (sender, args) =>
        {
            
            OnAdClosedEvent.Invoke();
        };
        rewardedAd.OnUserEarnedReward += (sender, args) =>
        {
           
            OnUserEarnedRewardEvent.Invoke();
        };
        rewardedAd.OnAdDidRecordImpression += (sender, args) =>
        {
           
        };
        rewardedAd.OnPaidEvent += (sender, args) =>
        {
            string msg = string.Format("{0} (currency: {1}, value: {2}",
                                        "Rewarded ad received a paid event.",
                                        args.AdValue.CurrencyCode,
                                        args.AdValue.Value);
           
        };

        // Create empty ad request
        rewardedAd.LoadAd(CreateAdRequest());
    }

    public void ShowRewardedAd()
    {
        if (rewardedAd != null)
        {
            rewardedAd.Show();
        }
        else
        {
            
        }
    }

//    public void RequestAndLoadRewardedInterstitialAd()
//    {
//        PrintStatus("Requesting Rewarded Interstitial ad.");

//        // These ad units are configured to always serve test ads.
//#if UNITY_EDITOR
//        string adUnitId = "unused";
//#elif UNITY_ANDROID
//            string adUnitId = "ca-app-pub-3940256099942544/5354046379";
//#elif UNITY_IPHONE
//            string adUnitId = "ca-app-pub-3940256099942544/6978759866";
//#else
//            string adUnitId = "unexpected_platform";
//#endif

//        // Create an interstitial.
//        RewardedInterstitialAd.LoadAd(adUnitId, CreateAdRequest(), (rewardedInterstitialAd, error) =>
//        {
//            if (error != null)
//            {
//                PrintStatus("Rewarded Interstitial ad load failed with error: " + error);
//                return;
//            }

//            this.rewardedInterstitialAd = rewardedInterstitialAd;
//            PrintStatus("Rewarded Interstitial ad loaded.");

//            // Register for ad events.
//            this.rewardedInterstitialAd.OnAdDidPresentFullScreenContent += (sender, args) =>
//            {
//                PrintStatus("Rewarded Interstitial ad presented.");
//            };
//            this.rewardedInterstitialAd.OnAdDidDismissFullScreenContent += (sender, args) =>
//            {
//                PrintStatus("Rewarded Interstitial ad dismissed.");
//                this.rewardedInterstitialAd = null;
//            };
//            this.rewardedInterstitialAd.OnAdFailedToPresentFullScreenContent += (sender, args) =>
//            {
//                PrintStatus("Rewarded Interstitial ad failed to present with error: " +
//                                                                        args.AdError.GetMessage());
//                this.rewardedInterstitialAd = null;
//            };
//            this.rewardedInterstitialAd.OnPaidEvent += (sender, args) =>
//            {
//                string msg = string.Format("{0} (currency: {1}, value: {2}",
//                                            "Rewarded Interstitial ad received a paid event.",
//                                            args.AdValue.CurrencyCode,
//                                            args.AdValue.Value);
//                PrintStatus(msg);
//            };
//            this.rewardedInterstitialAd.OnAdDidRecordImpression += (sender, args) =>
//            {
//                PrintStatus("Rewarded Interstitial ad recorded an impression.");
//            };
//        });
//    }

//    public void ShowRewardedInterstitialAd()
//    {
//        if (rewardedInterstitialAd != null)
//        {
//            rewardedInterstitialAd.Show((reward) =>
//            {
//                PrintStatus("Rewarded Interstitial ad Rewarded : " + reward.Amount);
//            });
//        }
//        else
//        {
//            PrintStatus("Rewarded Interstitial ad is not ready yet.");
//        }
//    }

    #endregion

    #region APPOPEN ADS

    public bool IsAppOpenAdAvailable
    {
        get
        {
            return (!isShowingAppOpenAd
                    && appOpenAd != null
                    && DateTime.Now < appOpenExpireTime);
        }
    }

    public void RequestAndLoadAppOpenAd()
    {
       
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = "ca-app-pub-3940256099942544/3419835294";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/5662855259";
#else
        string adUnitId = "unexpected_platform";
#endif
        // create new app open ad instance
        AppOpenAd.LoadAd(adUnitId,
                         ScreenOrientation.Portrait,
                         CreateAdRequest(),
                         OnAppOpenAdLoad);
    }

    private void OnAppOpenAdLoad(AppOpenAd ad, AdFailedToLoadEventArgs error)
    {
        if (error != null)
        {
           
            return;
        }

       
        this.appOpenAd = ad;
        this.appOpenExpireTime = DateTime.Now + APPOPEN_TIMEOUT;
    }

    public void ShowAppOpenAd()
    {
        if (!IsAppOpenAdAvailable)
        {
            return;
        }

        // Register for ad events.
        this.appOpenAd.OnAdDidDismissFullScreenContent += (sender, args) =>
        {
          
            isShowingAppOpenAd = false;
            if (this.appOpenAd != null)
            {
                this.appOpenAd.Destroy();
                this.appOpenAd = null;
            }
        };
        this.appOpenAd.OnAdFailedToPresentFullScreenContent += (sender, args) =>
        {
          
            isShowingAppOpenAd = false;
            if (this.appOpenAd != null)
            {
                this.appOpenAd.Destroy();
                this.appOpenAd = null;
            }
        };
        this.appOpenAd.OnAdDidPresentFullScreenContent += (sender, args) =>
        {
            
        };
        this.appOpenAd.OnAdDidRecordImpression += (sender, args) =>
        {
            
        };
        this.appOpenAd.OnPaidEvent += (sender, args) =>
        {
            string msg = string.Format("{0} (currency: {1}, value: {2}",
                                        "App Open ad received a paid event.",
                                        args.AdValue.CurrencyCode,
                                        args.AdValue.Value);
           
        };

        isShowingAppOpenAd = true;
        appOpenAd.Show();
    }

    #endregion


    #region AD INSPECTOR


    #endregion

    #region Utility

    #endregion
}
